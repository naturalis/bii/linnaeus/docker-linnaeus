#!/bin/sh
set -eu

export IMAGE_VERSION=build_"$CI_COMMIT_REF_SLUG"

mkdir -p "qa/database/storage"
chown 999:999 "qa/database/storage"

docker pull "$CI_REGISTRY_IMAGE/traefik:$IMAGE_VERSION"
docker pull "$CI_REGISTRY_IMAGE/docker-proxy:$IMAGE_VERSION"
docker pull "$CI_REGISTRY_IMAGE/nginx:$IMAGE_VERSION"
docker pull "$CI_REGISTRY_IMAGE/php-fpm:$IMAGE_VERSION"
docker pull "$CI_REGISTRY_IMAGE/php-dev:$IMAGE_VERSION"
docker pull "$CI_REGISTRY_IMAGE/mariadb:$IMAGE_VERSION"

docker compose -f "$PWD/qa/docker-compose.yml" up -d

sleep 5

docker run --rm -t -v "$PWD/qa/postman":/etc/newman --network qa_web postman/newman:5-alpine run smoke-test.json --insecure

docker compose -f "$PWD/qa/docker-compose.yml" -f "$PWD/qa/docker-compose.dev.yml" up -d

sleep 5

docker run --rm -t -v "$PWD/qa/postman":/etc/newman --network qa_web postman/newman:5-alpine run smoke-test.json --insecure
