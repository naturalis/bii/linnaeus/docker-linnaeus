#!/bin/sh
set -eu

if [ -z ${1+x} ]; then
  echo Missing name argument
  exit 1
fi
if [ -z ${2+x} ]; then
  echo Missing image:tag argument
  exit 2
fi
name=$1
image=$2

docker pull "$image"
docker tag "$image" "$CI_REGISTRY_IMAGE/$name:build_$CI_COMMIT_REF_SLUG"
docker push "$CI_REGISTRY_IMAGE/$name:build_$CI_COMMIT_REF_SLUG"
